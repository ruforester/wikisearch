/// <reference path="../typings/main.d.ts" />
var container;

function parseJSON(data) {
    
    if ($('#result').length) {
        $('#result').fadeOut('slow');
         $('#result').remove();
     }
     
    var htmlStr = '<ul id="result" class="list-unstyled">';
    
    for (var i = 0; i < data[1].length; i++) {
        htmlStr += '<a href="' + data[3][i] + '" target="_blank"><li class="roundCorners">' + data[1][i] + '<p class="text-justify">' + data[2][i] + '</p></li></a>';
    }
    htmlStr += '</ul>';
    
    $(htmlStr).hide().appendTo(container).fadeIn('slow');
    
    $('#result').on('mouseenter', 'li', function () {
                    $(this).toggleClass('bg-info');
                });
                $('#result').on('mouseleave', 'li', function () {
                    $(this).toggleClass('bg-info');
                })
    
   // return htmlStr;
}


$(document).ready(function() {
 
 $('#searchbox').focus();  
 container = $('.container');
//  result.hide();  
    
  $('#searchbox').keypress(function (e) {
     
     //result.fadeOut('slow');
     if (e.which == 13) {
         $(this).autocomplete('close');
         e.preventDefault();
         $.ajax({
            url: "http://en.wikipedia.org/w/api.php",
            dataType: "jsonp",
            data: {
                'action': "opensearch",
                'format': "json",
                'search': $(this).val()
                // 'action': "query",
                // 'format': "json",
                // 'titles': ui.item.value,
                // 'prop': "extracts",
                // 'exsentences': "3",
                // 'exsectionformat': "plain"
            },
            success: function(data) {
                
                parseJSON(data);
                //container.append(parseJSON(data)).fadeIn('slow');
                //result.fadeIn('slow');
                //result.show();
                
            }
        });
     } 
  })  
    
    
    $("#searchbox").autocomplete({
    source: function(request, response) {
        //result.fadeOut('slow');
        //console.log(request.term);
        $.ajax({
            url: "http://en.wikipedia.org/w/api.php",
            dataType: "jsonp",
            data: {
                'action': "opensearch",
                'format': "json",
                'search': request.term
            },
            success: function(data) {
                response(data[1]);
            }
        });
    },
    select: function (event, ui) {
        $(this).val(ui.item.value);
        $.ajax({
            url: "http://en.wikipedia.org/w/api.php",
            dataType: "jsonp",
            data: {
                'action': "opensearch",
                'format': "json",
                'search': ui.item.value
                // 'action': "query",
                // 'format': "json",
                // 'titles': ui.item.value,
                // 'prop': "extracts",
                // 'exsentences': "3",
                // 'exsectionformat': "plain"
            },
            success: function(data) {
                parseJSON(data);
                //container.append(parseJSON(data)).fadeIn('slow');
                //result.show();
                //result.fadeIn('slow');
                // $('#result').on('mouseenter', 'li', function () {
                //     $(this).toggleClass('bg-info');
                // });
                // $('#result').on('mouseleave', 'li', function () {
                //     $(this).toggleClass('bg-info');
                // })
            }
        });
    }

});
});

// /w/api.php?action=query&format=json&prop=extracts&titles=Main+Page&exsentences=3&exsectionformat=plain